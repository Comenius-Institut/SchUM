# SchUM

## Projekt Antisemitismusvorbeugung vor dem Hintergrund der Geschichte der SchUM-Städte

Das Projekt, welches vom [Denkendorfer Kreis](https://www.denkendorfer-kreis.de/) initiiert ist, ielt auf die Sensibilisierung von Schüler:innen in Bezug auf die Übernahme anti-jüdischer Stereotype, vornehmlich im Religionsunterricht, und dient damit der Vorbeugung des Antisemitismus. Dies geschieht anhand von religionspädagogischem Material, das von Pfarrer Markus Herb und Prof. Norbert Krüger erarbeitet wurde. Dieses Material wird in dem Projekt Antisemitismusvorbeugung vor dem Hintergrund der Geschichte der SchUM-Städte für verschiedene Schultypen und Altersstufen weiterentwickelt und dann durch die Online-Plattform rpi-virtuell.de im gesamten deutschsprachigen Raum verbreitet. 

In Abstimmung mit unseren Nutzungsbedingungen und den Leitlinien für Kooperationsprojekte sollen die technischen Voraussetzungen geschaffen werden, damit das entstehende Material über eine eigene Projekt-Webseite veröffentlicht wird und für Lehrkräfte sowie alle Interessierten zum Download bereitgestellt werden kann. 

Darüber hinaus sollen die Materialien im Materialpool verschlagwortet werden und in sozialen Medien verbreitet. 
Durch die Kooperation mit anderen Netzwerken und Plattformen beabsichtigen wir,  die Materialien auch auf Schul- und Bildungsservern verfügbar zu machen, um direkt in Kursformate und Taskcards integriert zu werden. 
